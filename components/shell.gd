extends KinematicBody2D

var direction = Vector2(0, 0)
var speed = 10
var impact = false
var damage = 1

func fire(direction):
	self.direction = direction.normalized()
	rotation = self.direction.angle()

func explode():
	var world = get_tree().get_root().get_node("game")
	var explode = load("res://components/explode.tscn").instance()
	explode.position.x = position.x 
	explode.position.y = position.y
	explode.emitting = true
	world.add_child(explode)
	queue_free()
	yield(get_tree().create_timer(1.0), "timeout")	
	explode.queue_free()

		
func _physics_process(delta):
	if !impact:	
		var collision_info = move_and_collide(direction * speed)
		if collision_info:
			impact = true
			var collision_point = collision_info.position
			explode()
			print(collision_info)
			if collision_info.collider.get("hp"):
				print(collision_info.collider.hp)
				collision_info.collider.hit(damage)
#				collision_info.collider.hp = collision_info.collider.hp - damage
			print(collision_info.collider.name)
			hide()
		
