extends Node2D



# Called when the node enters the scene tree for the first time.
func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	for x in range(stats.trainings_completed + 1):
		var buggy = load("res://components/buggy.tscn").instance()
		buggy.position.x = rng.randi_range(100, 3000)
		buggy.position.y = rng.randi_range(100, 1600)
		buggy.add_to_group("enemies")
		add_child(buggy)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
