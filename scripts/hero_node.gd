extends KinematicBody2D

signal death

var advance = Vector2(0, 0)
var dead = false
var target_rotation = 0
var target_direction = Vector2(0, 0)
var move_speed = stats.hero.spd
var is_hero = true
var max_hp = stats.hero.max_hp
var hp = stats.hero.max_hp
var damage = stats.hero.damage

onready var game = get_tree().get_root().get_node("game")

func _ready():
	update()

func save():
	stats.hero.max_hp = max_hp
	stats.hero.damage = damage
	stats.hero.spd = move_speed

func update():
	game.get_node("gui/hud/hero/hp_value").text = str(hp)
	game.get_node("gui/hud/hero/dmg_value").text = str(damage)
	game.get_node("gui/hud/hero/spd_value").text = str(move_speed)

func hit(damage):
	hp -= damage
	game.get_node("gui/hud/hero/hp_value").text = str(hp)
	if hp <= 0:
		hp = 0
		dead = true
		emit_signal("death")
		hide()
#		queue_free()

func fire(direction):
	var world = get_tree().get_root().get_node("game")
	var shell = load("res://components/shell.tscn").instance()
	shell.damage = damage
	shell.fire(direction)
	print(direction)
	shell.position.x = position.x + direction.x * 170
	shell.position.y = position.y + direction.y * 170
	world.add_child(shell)
	game.get_node("pew").play()


func _input(event):
	if dead:
		return
	if event.is_action("ui_down") &&  event.is_pressed() && !event.is_echo():
		advance.y = move_speed
	if event.is_action("ui_up") && event.is_pressed() && !event.is_echo():
		advance.y = -move_speed
	if event.is_action("ui_left") && event.is_pressed() && !event.is_echo():
		advance.x = -move_speed
	if event.is_action("ui_right") && event.is_pressed() && !event.is_echo():
		advance.x = move_speed
	if event.is_action("ui_down") &&  !event.is_pressed() && !event.is_echo():
		advance.y = 0
	if event.is_action("ui_up") && !event.is_pressed() && !event.is_echo():
		advance.y = 0
	if event.is_action("ui_left") && !event.is_pressed() && !event.is_echo():
		advance.x = 0
	if event.is_action("ui_right") && !event.is_pressed() && !event.is_echo():
		advance.x = 0
	if event.is_action("fire") && !event.is_pressed() && !event.is_echo():
		fire(target_direction)

func _physics_process(delta):
	if advance != Vector2.ZERO:
		target_rotation = advance.angle()
	if !dead:
		var collision_info = move_and_collide(advance)
		if collision_info:
			var collision_point = collision_info.position
			print(collision_info.collider.name)
		rotation = lerp(rotation, target_rotation, 0.08)
	var moug = get_global_mouse_position()

	var cannon_rotation = (moug - position).angle() - rotation
	$cannon.rotation = cannon_rotation
	target_direction = (moug - position).normalized()
