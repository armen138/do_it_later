extends Node2D

signal win
signal lose

var game_over = false

func _ready():
	var enemies = get_tree().get_nodes_in_group ("enemies")
	for enemy in enemies:
		enemy.connect("death", self, "enemy_died")
	var heroes = get_tree().get_nodes_in_group ("heroes")
	for hero in heroes:
		hero.connect("death", self, "hero_died")

func hero_died():
	print("hero died")
	var heroes = get_tree().get_nodes_in_group ("heroes")
	var survivor = false
	for hero in heroes:
		if not hero.dead:
			survivor = true
	if !survivor:
		lose()
	$explode.play()

func enemy_died():
	var enemies = get_tree().get_nodes_in_group ("enemies")
	var survivor = false
	for enemy in enemies:
		if not enemy.dead:
			survivor = true
	if !survivor:
		win()
	$explode.play()
		
func win():
	print("End Game")
	emit_signal("win")
	$gui/win.show()
	stats.trainings_completed += 1
	$victory.play()
	game_over = true
	var heroes = get_tree().get_nodes_in_group ("heroes")
	for hero in heroes:
		if not hero.dead:
			hero.save()

func lose():
	print("lose")
	emit_signal("lose")
	$game_over.play()
	$gui/lose.show()
	game_over = true

func _input(event):
	if game_over:
		yield(get_tree().create_timer(4.0), "timeout")
		get_tree().change_scene("res://scenes/question.tscn")
