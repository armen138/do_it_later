extends Node2D

var top = 50
var going_up = true
var bounciness = 0.2
var consumed = false

func _physics_process(delta):
	if !consumed:
		if going_up:
			$icon.position = lerp($icon.position, Vector2(0, -top), bounciness)
			if $icon.position.is_equal_approx(Vector2(0, -top)):
				going_up = false
		else:
			$icon.position = lerp($icon.position, Vector2(0, 0), bounciness)
			if $icon.position.is_equal_approx(Vector2(0, 0)):
				going_up = true
		var bodies = $area.get_overlapping_bodies() 
		if bodies.size() > 0:
			print(bodies)
			for body in bodies:
				if body.get("is_hero"):
					print("Hero picked up health")
					consumed = true
					body.max_hp += 1
					body.hp += 1
					body.damage +=1
					body.move_speed += 1
					body.update()
					queue_free()
