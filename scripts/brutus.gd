extends KinematicBody2D

signal death

var hp = 200
var dead = false
var detection_distance = 800
var firing_distance = 2000
var damage = 10
var fire_cooldown = 1000
var target_rotation = 0
var last_fire = 0
var speed = 4

onready var game = get_tree().get_root().get_node("game")
onready var hero = get_tree().get_root().get_node("game/training_field/hero")

func _ready():
	game.get_node("gui/hud/brutus/hp_value").text = str(hp)

func fire(direction):
	last_fire = OS.get_ticks_msec()
	var world = get_tree().get_root().get_node("game")
	var shell = load("res://components/brutus_shell.tscn").instance()
	shell.fire(direction)
	print(direction)
	shell.position.x = position.x + direction.x * 250
	shell.position.y = position.y + direction.y * 250
	shell.damage = damage
	shell.speed = 20
	world.add_child(shell)
	game.get_node("explode").play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var now = OS.get_ticks_msec()
	if dead:
		return
	if not dead and hp <= 0:
		dead = true
		emit_signal("death")
		queue_free()
		return
	if not hero:
		return
	if hero.dead:
		return
	var distance_to_target = position.distance_to(hero.position)
	target_rotation = (hero.position - position).angle()
	if distance_to_target < detection_distance:
		rotation = lerp(rotation, target_rotation, 0.04)
	var nose = Vector2(cos(rotation), sin(rotation))
	if distance_to_target < firing_distance and now - last_fire > fire_cooldown:
		fire((hero.position - position).normalized())
	if distance_to_target < detection_distance and distance_to_target > firing_distance:
		move_and_collide(nose * speed)
#	cannon_rotation = 
	$cannon.rotation = target_rotation - rotation
#		target_direction = (moug - position).normalized()	

func hit(damage):
	hp -= damage
	game.get_node("gui/hud/brutus/hp_value").text = str(hp)
