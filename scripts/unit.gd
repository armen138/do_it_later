class_name Unit
extends Resource

export(String) var name
export(int) var hp
export(int) var max_hp
export(int) var damage
export(int) var spd

export(Array, Resource) var items

func _ready():
	pass # Replace with function body.
