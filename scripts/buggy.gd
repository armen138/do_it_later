extends KinematicBody2D

signal death

var hp = 10
var dead = false
var detection_distance = 1000
var firing_distance = 500
var damage = 1
var fire_cooldown = 500
var target_rotation = 0
var last_fire = 0
var speed = 10

onready var hero = get_tree().get_root().get_node("game/training_field/hero")
onready var field = get_tree().get_root().get_node("game/training_field")
onready var game = get_tree().get_root().get_node("game")

func _ready():
	pass # Replace with function body.

func fire(direction):
	last_fire = OS.get_ticks_msec()
	var world = get_tree().get_root().get_node("game")
	var shell = load("res://components/bullet.tscn").instance()
	shell.fire(direction)
	print(direction)
	shell.position.x = position.x + direction.x * 70
	shell.position.y = position.y + direction.y * 70
	shell.damage = damage
	shell.speed = 40
	world.add_child(shell)
	game.get_node("put").play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var now = OS.get_ticks_msec()
	if not dead and hp <= 0:
		dead = true
		emit_signal("death")
		var drop = load("res://components/hp_pickup.tscn").instance()
		drop.position = position
		field.add_child(drop)
		queue_free()
	if hero.dead:
		return
	var distance_to_target = position.distance_to(hero.position)
	if distance_to_target < detection_distance:
		target_rotation = (hero.position - position).angle()
	rotation = lerp(rotation, target_rotation, 0.04)
	var nose = Vector2(cos(rotation), sin(rotation))
	if distance_to_target < firing_distance and now - last_fire > fire_cooldown:
		fire(nose)
	if distance_to_target < detection_distance and distance_to_target > firing_distance:
		move_and_collide(nose * speed)
	

func hit(damage):
	hp -= damage
